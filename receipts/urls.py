from receipts.views import (
    receipt_list,
    create_new_receipt,
    category_list,
    account_list,
)
from django.urls import path

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_new_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
]
